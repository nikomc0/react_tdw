import React, { Component } from 'react';
import { Provider } from 'rebass';
import { Route } from 'react-router-dom';
import './App.css';

import Home from './components/Home';
import Podcast from './components/Podcast';
import About from './components/About';
import LatestEpisode from './components/LatestEpisode';
import Quotes from './components/Quotes';
import Instagram from './components/Instagram';

class App extends Component {
  render() {
    return (
      <Provider className="App">
        <main>
          <Route exact path="/" component={Home} />
          <Route path="/podcast" component={Podcast} />
          <Route path="/about" component={About} />
          <Route path="/latest" component={LatestEpisode} />
          <Route path="/quotes" component={Quotes} />
          <Route path="/photos" component={Instagram} />
        </main>
      </Provider>
    );
  }
}

export default App;
