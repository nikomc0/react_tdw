var quotes = [
	{	name: "Sydney", title: "Your podcasts get me through my work week!", body: "Your podcasts get me through my work week! Sending much love and power to the both of you! Love the stories and laughs and will be listening to you both for a long time to come." },
	{	name: "Joanna", title: "It’s pretty awesome...", body: "It’s pretty awesome to feel like two people that you don’t even know from a can of paint, can feel like you’re with your two best friends at some dive bar, beers in hand." },
	{	name: "Michael", title: "Holy Hell!", body: "Holy Hell! I feel like I’ve discovered my potty-mouthed new best friends. I’m not sure how y’all can be fuckin’ better, but I’ll be listening to find out. Thanks for improving my life." },
	{	name: "Vanessa", title: "The conversation is natural & hilarious.", body: "The conversation is natural & hilarious. I feel like I’m in the room with Jenn & Dom, my cool new bffs. Their stories will have you trying not to spew coffee in your keyboard." }
];

var ItunesAppReviews = require('itunes-app-reviews');
var iTunesAppReviews = new ItunesAppReviews();

// your app ID, country, limit page number
iTunesAppReviews.getReviews('934998314', 'us', 1);
// success
iTunesAppReviews.on('data', function(reviews){
	return reviews.forEach(function(review){
		quotes.push({ 
			name: review.author[0].name.join(), 
			title: review.title.join(), 
			body: review.content[0]._
		})
	})
});

// console.log(quotes)

export default quotes
