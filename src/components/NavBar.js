import React, { Component } from 'react';
import { HashLink as Link } from 'react-router-hash-link';

class NavBar extends Component {
	render (){
		return (
			<nav id="navbar" className="navbar navbar-expand-lg navbar-dark sticky-top">
			  <Link className="navbar-brand" to="/"><h2>TDW</h2></Link>
			  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			    <span className="navbar-toggler-icon"></span>
			  </button>

			  <div className="collapse navbar-collapse" id="navbarSupportedContent">
			    <ul className="navbar-nav ml-auto">
			      <li className="nav-item active">
			        <Link className="nav-link" to="/">Home <span className="sr-only">(current)</span></Link>
			      </li>
			      <li className="nav-item">
			        <Link className="nav-link" to="/podcast">Podcast</Link>
			      </li>
			      <li className="nav-item">
			        <Link className="nav-link" to="/#about">About</Link>
			      </li>
			      <li className="nav-item">
			        <Link className="nav-link" to="/#photos">Instagram</Link>
			      </li>
			    </ul>
			  </div>
			</nav>
		);
	}
}

export default NavBar
