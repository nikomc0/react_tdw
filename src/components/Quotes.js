import React, { Component } from 'react';
import quotes from '../data/quotes';
import '../index.css';

var ItunesAppReviews = require('itunes-app-reviews');
var iTunesAppReviews = new ItunesAppReviews();

class Quotes extends Component {
	constructor(props){
		super(props);
		this.state = {quotes: quotes};
	}

	componentDidMount(){
		this.getQuotes();
	}

	getQuotes(){
		var currentQuotes = this;

		iTunesAppReviews.getReviews('934998314', 'us', 1);
		// success
		iTunesAppReviews.on('data', function(reviews){
			var quotes = [
				{	name: "Sydney", title: "Your podcasts get me through my work week!", body: "Your podcasts get me through my work week! Sending much love and power to the both of you! Love the stories and laughs and will be listening to you both for a long time to come." },
				{	name: "Joanna", title: "It’s pretty awesome...", body: "It’s pretty awesome to feel like two people that you don’t even know from a can of paint, can feel like you’re with your two best friends at some dive bar, beers in hand." },
				{	name: "Michael", title: "Holy Hell!", body: "Holy Hell! I feel like I’ve discovered my potty-mouthed new best friends. I’m not sure how y’all can be fuckin’ better, but I’ll be listening to find out. Thanks for improving my life." },
				{	name: "Vanessa", title: "The conversation is natural & hilarious.", body: "The conversation is natural & hilarious. I feel like I’m in the room with Jenn & Dom, my cool new bffs. Their stories will have you trying not to spew coffee in your keyboard." }
			];
			
			reviews.forEach(function(review){
				quotes.push({ 
					name: review.author[0].name.join(), 
					title: review.title.join(), 
					body: review.content[0]._
				})
			})

			currentQuotes.setState({ 
				quotes: quotes 
			});
		});
	}

	render (){
		// console.log(this.state.quotes)
		return (
			<section className="container-fluid quotes">
				<div id="carouselExampleSlidesOnly" className="carousel slide" data-ride="carousel">
				  <div className="carousel-inner">
				    <div className="carousel-item active">
				    	<div className="row">
								<div className="col">
									<blockquote className="blockquote">
										<h1>"I have been laughing out loud at your Brady’s Great Barbecue 
										and Catering call-out @ThisDamnWorldSF."</h1>
									</blockquote>
								</div>
							</div>
							<div className="row">
								<div className="col">
									<h2>Fizz</h2>
								</div>
							</div>
				    </div>
			    	{
			    		this.state.quotes.map( (quote, index) =>
								<div key={index} className="carousel-item">
									<div className="row">
										<div className="col">
											<blockquote className="blockquote"><h1>"{quote.body}"</h1></blockquote>
										</div>
									</div>
									<div className="row">
										<div className="col">
											<h2>{quote.name}</h2>
										</div>
									</div>
								</div>
			    		)
			    	}
				  </div>
				</div>
			</section>
		);
	}
}

export default Quotes
