import React, { Component } from 'react';
import { Provider } from 'rebass';
import LatestEpisode from './LatestEpisode';
import Header from './Header';
import NavBar from './NavBar';
import About from './About';
import Quotes from './Quotes';
import Instagram from './Instagram';

class Home extends Component { 

 render() {
  return ( 
    <Provider>
    	<Header />

			<NavBar />

			<LatestEpisode />

			<About />
			
			<Quotes />
			
			<Instagram />
		
		</Provider>
		);
	}
}

export default Home;
