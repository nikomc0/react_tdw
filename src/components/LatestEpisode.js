import React, { Component } from 'react';
import episodes from '../data/episodes';
import '../index.css'
import vtt from '../vtt/ep1.vtt'

let parser = require('rss-to-json');
		// console.log(episodes[0])

class LatestEpisode extends Component {
	constructor(props){
		super(props);
		this.state = {latest: null, subtitle: null, description: null, url: null, ep: null};
	}

	componentDidMount(){
		var current = this;
		parser.load('https://thisdamnworld.libsyn.com/rss', function(err, rss){
			current.setState({ 
				latest: rss.items[0].title,
				description: rss.items[0].itunes_subtitle, 
				url: rss.items[0].url + "#t=20",
				ep: rss.items.length
			})
		})
	}

	getLatestEpisode(episodes){
		var current = this;
		current.setState({
			latest: episodes[0]
		});
		console.log(episodes)
	}


	render (){
		return (
			<section id="latest">
			<div className="jumbotron jumbotron-fluid latest-episode">
			  <div className="container py-5">
			    <h1 className="display-5 latest-episode-title">Episode #{this.state.ep} - {this.state.latest}</h1>
			    <p className="lead">{this.state.description}</p>
					<div>
						<audio src={this.state.url} controls="controls" width="400">
							<track default kind="chapters" src={vtt} />
						</audio>
					</div>
			  </div>
			</div>
			</section>
		);
	}
}

export default LatestEpisode;
