import React, { Component } from 'react';
import Instafeed from 'react-instafeed';
import '../index.css';

class Instagram extends Component {
	shouldComponentUpdate() {
		return false;
	}
	render (){
		var instafeedTarget = 'instafeed';
		return (
			<section id="photos">
				<div className="my-3 upcase">
					<h2>Follow Us on Instagram</h2>
				</div>
				<div id={instafeedTarget} className="row no-gutters justify-content-center">
		      <Instafeed
		        limit='18'
		        ref='instafeed'
		        resolution='standard_resolution'
		        sortBy='most-recent'
		        target={instafeedTarget}
		        template='<a href="{{link}}">
        								<img class="instafeed__item__background" src="{{image}}"/>
        							</a>'
		        userId='1643505693'
		        clientId='ceba77a2bc564a03babaadd10a79529e'
		        accessToken='1643505693.ceba77a.cc82ab38564f4af58ffe7f41c8bf9ac6'
		       />
	      </div>
	    </section>
		);
	}
}

export default Instagram;
