import React, { Component } from 'react';

class Pagination extends Component {
	constructor(props){
		super(props);
		this.state = { lastPage: 5, firstPage: 0, currentSet: 1, maxPages: 5, isActive: 5 };
	}

	prevPage(currentPage){
		if (currentPage - 1  <= this.state.firstPage && currentPage > 1){
			var lastPage = this.state.lastPage - this.state.maxPages;
			var firstPage = lastPage - this.state.maxPages;
			this.setState({
				lastPage: lastPage,
				firstPage: firstPage
			});
		}
		this.props.prevPage(currentPage);
	}

	nextPage(currentPage, totalPages){
		if (currentPage >= this.state.lastPage){
			var lastPage = this.state.lastPage + this.state.maxPages;
			var firstPage = lastPage - this.state.maxPages;
			this.setState({
				lastPage: lastPage,
				firstPage: firstPage
			});
		}
  	this.props.nextPage(currentPage, totalPages);
  }

	render (){
		const currentPage = this.props.currentPage;
		const lastPage = this.state.lastPage;
		const firstPage = this.state.firstPage;
		console.log({firstPage, lastPage, currentPage});
		var pages = this.props.pages.slice(this.state.firstPage, this.state.lastPage);

		return (
			<nav aria-label="page navigation">
				<ul className="pagination justify-content-center">
					<li className="page-item">
		      	<a className="page-link" onClick={() => this.prevPage(this.props.currentPage)}>Previous</a>
		   		</li>
				{
					pages.map ( (page, index) =>
						<div key={index}>
							<li className={pages.indexOf(this.props.currentPage) === index ? 'page-item active' : 'page-item'} onClick={() => this.props.setPage(page)}><a className="page-link">{page}</a></li>
						</div>
					)
				}
					<li className="page-item">
						<a className="page-link" onClick={() => this.nextPage(this.props.currentPage, this.props.totalPages)}>Next</a>
					</li>
				</ul>
			</nav>
		);
	}
}

export default Pagination;
