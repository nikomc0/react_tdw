import React, { Component } from 'react';
import { HashLink as Link } from 'react-router-hash-link';
import '../index.css';

class Header extends Component {
	constructor(props){
		super(props);
		this.state = {twitter: "https://twitter.com/thisdamnworldsf", instagram: "https://www.instagram.com/thisdamnworld/", facebook: "https://www.facebook.com/thisdamnworld"}
	}

	render() {
		return (
			<section className="jumbotron jumbotron-fluid header">
				<h1 className="display-2 header-text">THIS <span className="theme-color">DAMN</span> WORLD</h1>
				<div className="row justify-content-center">
					<a href={this.state.facebook}><i className="fab fa-facebook icon fa-3x social"></i></a>
					<a href={this.state.instagram}><i className="fab fa-instagram icon fa-3x social"></i></a>
					<a href={this.state.twitter}><i className="fab fa-twitter icon fa-3x social"></i></a>
				</div>
				<Link to="/#latest"><button type="button" className="btn btn-outline-danger">MORE</button></Link>
			</section>
		);
	}
}

export default Header