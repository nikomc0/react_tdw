import React from 'react';
import '../index.css';
import Dom from '../assets/DomTDW.png';
import Aya from '../assets/AyaTDW.jpg';
import Matt from '../assets/MattTDW.jpg';

const About = () => (
	<section id="about" className="about container-fluid">
		<div className="my-5">
			<h2>THIS <span className="theme-color">DAMN</span> WORLD</h2>
			<h4>IS A LONG FORM CONVERSATIONAL PODCAST WITH TOPICS RANGING FROM <span className="theme-color">NEWS</span>, <span className="theme-color">LIFE</span>, 
			AND ALL THINGS <span className="theme-color">CREATIVE</span>. TDW AIMS TO BE THE ENTERTAINING OUTLET FOR YOUR DOSE OF HUMOR, 
			EDUCATION, AND INSPIRATION.
			</h4>
		</div>
		<div className="row mb-sm-5 justify-content-center">
			<div className="col-sm-4">
				<img src={Dom} alt="Dom" width="300"/>
				<div><h2>DOM</h2></div>
				<div className="p-1"><h3 className="theme-color">PRODUCER/HOST</h3></div>
			</div>
			<div className="col-sm-3">
				<img src={Aya} alt="Aya" width="300"/>
				<div><h2>AYA</h2></div>
				<div className="p-1"><h3 className="theme-color">SHOWRUNNER/HOST</h3></div>
			</div>
			<div className="col-sm-4">
				<img src={Matt} alt="Matt" width="300"/>
				<div><h2>MATT</h2></div>
				<div className="p-1"><h3 className="theme-color">EDITOR/HOST</h3></div>
			</div>
		</div>
	</section>
);

export default About;