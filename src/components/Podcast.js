import React, { Component } from 'react';
import NavBar from './NavBar';
import Pagination from './Pagination';
import episodes from './../data/episodes';
import scrollToComponent from 'react-scroll-to-component';

class Podcast extends Component {
	constructor(props) {
		super(props);
		this.state = { episodes: episodes, results: 10, currentPage: 1, isActive: true };
		this.setPage = this.setPage.bind(this);
		this.nextPage = this.nextPage.bind(this);
	}

	componentDidMount() {
    scrollToComponent(this.Podcast, { offset: 0, align: 'top', duration: 500, ease:'inCirc'});
  }

  scrollToTop(){
  	scrollToComponent(this.Podcast, { offset: 0, align: 'top', duration: 500, ease:'inCirc'});
  }

  setPage(page){
  	this.setState({
  		currentPage: page
  	});
  	this.scrollToTop();
  }

  prevPage(page){
  	if (page > 1){
  		this.setState({
  			currentPage: page - 1
  		});
  	}
		this.scrollToTop();
  }

  nextPage(currentPage, totalPages){
  	var nextPage = currentPage + 1;
  	if (nextPage <= totalPages){
  		this.setState({
  			currentPage: nextPage 
  		});
  	}
  	this.scrollToTop();
  }

	render() {
		const totalPages = Math.ceil((this.state.episodes.length - 1) / this.state.results);
		const lastEpisodeCurrentPage = this.state.currentPage * this.state.results;
    const firstEpisodeCurrentPage = lastEpisodeCurrentPage - this.state.results;
    const currentPageEpisodes = this.state.episodes.slice(firstEpisodeCurrentPage, lastEpisodeCurrentPage);
		
		const pages = [];
		for (var i = 1; i <= totalPages; i++){
			pages.push(i);
		}
		
		return (
			<section className='podcast' ref={(section) => { this.Podcast = section; }}>
			<NavBar />
			  {
					currentPageEpisodes.map( (episode, index) => 
						<div key={index} className="container">
							<div className="row my-5">
								<div className="col-sm-6">
									<h5>Episode # {episode.ep}</h5>
									<h4>{episode.title}</h4>
									<p>{episode.itunes_subtitle}</p>
								</div>
								<div className="col-sm-6">	
									<h6>{episode.title}</h6>
									<audio src={episode.url} controls="controls" width="400">
									</audio>
								</div>
							</div>
							<hr/>				
						</div>	
					)	
				}
				<Pagination 
					totalPages={totalPages} 
					currentPage={this.state.currentPage} 
					pages={pages}
					setPage={this.setPage}
					nextPage={this.nextPage}
					prevPage={() => this.prevPage(this.state.currentPage)}
				/>
		  </section>
		);
	}
}

export default Podcast;
